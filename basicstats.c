#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
//Open File
FILE *open_file(char *file_name){
    if(fopen(file_name,"r") == NULL){//False open file
        printf("Error: Cannot Read the File!");
        exit(0);
    }
}

//Mean Function --> sum(single)/sizeof()
double mean_func(float *arr, int n){
    double sum = 0; //Initialize the sum = 0
    for(int i = 0; i < n; i++){ //For loop to add
        sum = sum + arr[i]; 
    }
    double mean = sum / n; //divide by n
    return mean;
}

//Standard Deviation Function --> sqrt(pow(single-mean))/sizeof()
double std_func(float *arr,int n){
    double std = 0; //Initialize the std = 0
    double mean = mean_func(arr,n); //Get the mean of the arr
    for (int i = 0; i < n;i++ ){
        std +=pow(arr[i] - mean, 2);//pow(single-mean))
    }
    return sqrt(std/n);
}

//Sort for the median_func
double sort(float *arr,int n){
    int i = 0, j = 0; // Initialize the comparable nums
    assert(arr); 
    for(i=0;i<n-1;i++){ // For loop to get the end of arr-1
        for(j=0;j<n-i-1;j++){ // For loop to get the end of arr-i-i
            if(arr[j]>arr[j+1]){ // If left > right
                double tmp = arr[j];// Swap the order
                arr[j] = arr[j+1];
                arr[j+1] = tmp;
            }
        }
    }
}

//Median, n%2 == 1 --> arr[n/2] or n%2 == 0 --> (arr[n/2-1]+arr[n/2])/2
double median_func(float *arr, int n){
    if(n%2 == 1){ // Odd situation 
        return (arr[n/2]); // Middle number
    }
    else{//Even situation
        double left = arr[n/2-1];
        double right = arr[n/2];
        return ((left + right)/2);// Mean of middle numbers

    }
}

// Mean Function
int main(int argc, char *argv[]){
    if (argc < 2){ //Argument vector, enter the file path
        printf("Enter the file path of:\n");
        return 0;
    }
    char *file_name = argv[1]; //Get the file_name
    FILE *file_open = open_file(file_name); //Open the File
    int n = 20; //The max number
    int length = 0; //The length of arr
    float tmp , *arr = (float *) malloc(n *sizeof(float)); //Give the address to arr

    while(!feof(file_open)){ //For loop to read file
        fscanf(file_open,"%f\n",(arr + length)); //Every line as a data
        length++; //One line one data_num_add
        if(length == n){ //If more than 20
            float *new_arr = (float *) malloc(n * 2 * sizeof(float));// Two address size
            memcpy(new_arr,arr,length * sizeof(float));
            free(arr);// Clear the original address
            arr = new_arr;
            n = n*2;
        }
    }

    fclose(file_open);// Close the file 
    double mean = mean_func(arr,length);// Mean func
    double std = std_func(arr,length);// Std func
    printf("The mean is: %f\n",mean);
    printf("The standard deviation is: %f\n",std);
    sort(arr,length);// Sort for the median compute
    double median = median_func(arr,length);
    printf("The median is: %f\n",median);
}