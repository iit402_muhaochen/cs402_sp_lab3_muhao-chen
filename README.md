# README #

· This README is for the operation of "Qucikly Statistic C Program".

### Qucikly Statistic C Program###

· C Program can save the memory and improve the speed for the data analysing, involving mean,median and standard deviation.
V1.0

### Build ###
· Firstly, we should make sure how many functions involved and the relation between these functions:
· FILE *open_file(char *file_name)
· double mean_func(float *arr, int n)
· double std_func(float *arr,int n)
· double sort(float *arr,int n)
· double median_func(float *arr, int n)
· The relation:
# 			  ---- mean_func
# open_file --- ---- mean_func ---- std_func
#              ---- sort ---- median_func
			  
· Mean Function --> sum(single)/sizeof()
· Standard Deviation Function --> sqrt(pow(single-mean))/sizeof()
· Median, n%2 == 1 --> arr[n/2] or n%2 == 0 --> (arr[n/2-1]+arr[n/2])/2

Besides:
· If the size is over than n=20, we can double the memory (malloc).(Save the memory)


### Set up ###

· Environment: ubuntu, Linux, gdb/gcc
· Set up directly with terminal:
· gcc basicstats.c(file path)

### Operation ###

· gcc basicstats.c -lm
· Test1(small data):
· ./static.exe small.txt
· Result_:
· The mean is: 85.776175
· The standard deviation is: 90.380406
· The median is: 67.469799

# Test2(large data):
· //Becasue this program save the memory, which use enlarge and clear memeory.
· ./static.exe large.txt
· Result_:
· The mean is: 2035.600000
· The standard deviation is: 1496.152858
· The median is: 1956.000000
